@tag
Feature: As a user of the Solicitudes application
  i will access the different categories of advances

  Background:
    Given The user accesses solicitudes website


  @successful
  Scenario: user successfully login to Solicitudes website
    When The user enters with their credentials
      | mail                           | pass       |
      | apps-ecosistemas@pragma.com.co | Pruebas22* |
    Then Validate sucessful message


  @failed
  Scenario: user was unable to login to Solicitudes website
    When The user enters with wrong credentials
      | mail                  |
      | estefa20039@gmail.com |
    Then user should see failed message"No se ha podido iniciar sesión"

