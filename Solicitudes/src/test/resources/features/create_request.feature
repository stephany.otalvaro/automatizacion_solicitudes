Feature:The user must be logged in to
  create the down payment in the Request application.

  Scenario: As the user creates a down payment request and validates the created request
    Given The user enters the application
    When The user enters with their credentials
      | mail                           | pass       |
      | apps-ecosistemas@pragma.com.co | Pruebas22* |
    And he enters the necessary data for the creation of the request and proceeds to the creation of the down payment
    Then he will see the message "Tu anticipo ha sido creado con éxito."