package pragma.solicitudes.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/long_in.feature",
        glue = "pragma.solicitudes.stepdefinitions",
        snippets = SnippetType.CAMELCASE,
        tags = "@failed")

public class LoginIn {
}
