package pragma.solicitudes.stepdefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.core.IsEqual;
import solicitudes.models.Credenciales;
import solicitudes.questions.FinalMessage;
import solicitudes.questions.SuccessfulLogin;
import solicitudes.tasks.FailedUser;
import solicitudes.tasks.LogIn;
import solicitudes.utils.Constant;
import solicitudes.utils.MyDriver;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class LoginInStepDefinitions {


    @Before
    public void setUp() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("stephany");
    }


    @Given("^The user accesses solicitudes website$")
    public void theUserAccessesSolicitudesWebsite() {
        OnStage.theActorInTheSpotlight().whoCan(BrowseTheWeb.with(MyDriver.aChromeDriver().inTheWebPage("https://d2y95fubzeqro6.cloudfront.net/login")));
    }

    @When("^The user enters with their credentials$")
    public void theUserEntersWithTheirCredentials(List<Map<String, String>> credenciales) {
        Credenciales cr = new Credenciales(credenciales);
        OnStage.theActorInTheSpotlight().attemptsTo(LogIn.pragma(cr));
    }

    @When("^The user enters with wrong credentials$")
    public void theUserEntersWithWrongCredentials(List<Map<String, String>> credenciales) {
        Credenciales cr = new Credenciales(credenciales);
        OnStage.theActorInTheSpotlight().attemptsTo(FailedUser.fail(cr));
    }


    @Then("^Validate sucessful message$")
    public void validateSucessfulMessage() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(SuccessfulLogin.login(), IsEqual.equalTo(Constant.LABEL_USER)));

    }

    @Then("^user should see failed message\"([^\"]*)\"$")
    public void userShouldSeeFailedMessage(String expectedMessage) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(FinalMessage.order(),
                IsEqual.equalTo(expectedMessage)));

    }


}
