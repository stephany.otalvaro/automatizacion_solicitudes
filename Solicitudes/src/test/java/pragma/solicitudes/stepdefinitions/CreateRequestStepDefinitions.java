package pragma.solicitudes.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import org.hamcrest.core.IsEqual;
import solicitudes.questions.SuccessRequest;
import solicitudes.tasks.CreateRequest;
import solicitudes.utils.MyDriver;

public class CreateRequestStepDefinitions {

    @Given("^The user enters the application$")
    public void theUserEntersTheApplication() {
        OnStage.theActorInTheSpotlight().whoCan(BrowseTheWeb.with(MyDriver.aChromeDriver().inTheWebPage("https://d2y95fubzeqro6.cloudfront.net/solicitaranticipo")));

    }

    @When("^he enters the necessary data for the creation of the request and proceeds to the creation of the down payment$")
    public void heEntersTheNecessaryDataForTheCreationOfTheRequestAndProceedsToTheCreationOfTheDownPayment() {
        OnStage.theActorInTheSpotlight().attemptsTo(CreateRequest.create());

    }

    @Then("^he will see the message \"([^\"]*)\"$")
    public void heWillSeeTheMessage(String expectedMessage) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(SuccessRequest.validate(),
                IsEqual.equalTo(expectedMessage)));

    }


}
