package solicitudes.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CreateRequest {

    public static final Target ADVANCE_PAYMENT_REQUEST = Target.the("Advance payment request").
            locatedBy("(//h2[@class=\"styles_title__VwCA6\"])[2]");
    public static final Target REASON = Target.the("Reason").
            locatedBy("//textarea[@name=\"observacionGeneral\"]");
    public static final Target BUSINESS = Target.the("Reason").
            located(By.id("react-select-:rh:-input"));
    public static final Target MARK = Target.the("Mark").
            located(By.id("react-select-:rj:-input"));
    public static final Target ACCEPT = Target.the("Accept").
            locatedBy("//button[@name=\"ant-request_button-accept\"]");
    public static final Target CITY = Target.the("Select City").
            locatedBy("(//label[text()=\"Ciudad de residencia\"]/div/div/div[1])/div/input");
    public static final Target DEPOSITE_DATE = Target.the("Deposite date").
            located(By.name("fechaConsignacion"));
    public static final Target APPROVER = Target.the("Select approver").
            located(By.id("react-select-:rq:-input"));
    public static final Target TRANSPORT = Target.the("Transport").
            locatedBy("//input[@name=\"costoTransporte\"]");
    public static final Target FOOD = Target.the("Food").
            locatedBy("//input[@name=\"costoAlimentacion\"]");
    public static final Target OTHERS = Target.the("Others").
            locatedBy("//input[@name=\"costoOtro\"]");
    public static final Target CURRENCY = Target.the("Select Currency").
            located(By.id("react-select-:rt:-input"));
    public static final Target ROUND_TRIP = Target.the("Round trip").
            locatedBy("//input[@name=\"ant-request_checkbox-roundtrip\"]");
    public static final Target ORIGIN = Target.the("Select City").
            locatedBy("(//label[text()=\"Origen\"]/div/div/div[1])/div/input");
    public static final Target DESTINATION = Target.the("Select destination").
            locatedBy("(//label[text()=\"Destino\"]/div/div/div[1])/div/input");
    public static final Target DEPARTURE_DATE = Target.the("Departure date").
            locatedBy("//input[@name=\"ant-request_inputdate-travel\"]");
    public static final Target RETURN_DATE = Target.the("Return date").
            locatedBy("//input[@name=\"ant-request_inputdate-travel-return\"]");
    public static final Target HOSTING_IN_CITY = Target.the("Hosting in city").
            locatedBy("(//label[text()=\"Ciudad\"]/div/div/div[1])/div/input");
    public static final Target ARRIVAL_DATE = Target.the("Arrival date").
            locatedBy("//label[text()=\"Fecha de llegada\"]/div/div/input");
    public static final Target DEPARTURE_DATE_HOSTING = Target.the("Departure date hosting").
            locatedBy("//label[text()=\"Fecha de salida\"]/div/div/input");
    public static final Target ADD = Target.the("Add").
            locatedBy("//button[@name=\"ant-request_button-add-hosting\"]");
    public static final Target CREATE_REQUEST = Target.the("Create request").
            locatedBy("//button[@name=\"ant-request_button-create-request\"]");
    public static final Target REQUEST_TAB = Target.the("request tab").
            locatedBy("//div[text()=\"Solicitudes\"]/ancestor::a");
    public static final Target REQUEST_NUMBER = Target.the("request number").
            locatedBy("(//div/button[text()=\"Mis solicitudes\"]/ancestor::div/span)[1]");
    public static final Target LAST_REQUEST = Target.the("last request").
            locatedBy("(//div[@class='styles_id__WNuXx'])[1]");
    public static final Target SUCCESS_REQUEST = Target.the("success request").
            locatedBy("//div[@data-color=\"success\"]");


}
