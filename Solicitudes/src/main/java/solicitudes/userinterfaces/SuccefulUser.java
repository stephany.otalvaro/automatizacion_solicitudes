package solicitudes.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SuccefulUser {
    public static final Target WELCOME = Target.the("Welcome").
            locatedBy("//button[@name='button_login']");
    public static final Target EMAIL = Target.the("Email").
            located(By.id("identifierId"));
    public static final Target NEXT = Target.the("Next").
            locatedBy("(//*/span[@jsname=\"V67aGc\"])[2]");
    public static final Target PASSWORD = Target.the("Password").
            locatedBy("//input[@name=\"password\"]");
    public static final Target NEXT_2 = Target.the("Next").
            locatedBy("(//*/span[@class=\"VfPpkd-vQzf8d\"])[2]");

    public static final Target TRY_ANOTHER_WAY = Target.the("try another way").
            locatedBy("(//*/span[@class=\"VfPpkd-vQzf8d\"])[2]");
    public static final Target TRY_ANOTHER_WAY_ALERT = Target.the("try another way alert").
            locatedBy("//button[text()=\"Probar otra manera\"]");
    public static final Target EIGHT_DIGIT_CODES = Target.the("eight digit codes").
            locatedBy("//li/div[contains(.,\"ocho\")]");

    public static final Target WRITE_A_CODE = Target.the("Write a code").
            located(By.id("backupCodePin"));
    public static final Target NEXT_3 = Target.the("Next").
            locatedBy("(//span[@class=\"VfPpkd-vQzf8d\"])[1]");
    public static final Target MESSAGE_FINAL = Target.the("Message final").
            locatedBy("//*/span[@class=\"styles_header-logo__DG0wI\"]");
    public static final Target ERROR_LOGIN = Target.the("select error message")
            .located(By.id("headingText"));

    public static final Target FAIL_ALERT = Target.the("fail alert").
            locatedBy("//span[text()=\"Se han producido demasiados intentos fallidos\"]");


}
