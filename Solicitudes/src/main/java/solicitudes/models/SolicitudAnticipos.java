package solicitudes.models;

import java.util.Map;

public class SolicitudAnticipos {

    private String reasonForTheRequest;
    private String business;
    private String brand;
    private String hometown;
    private String depositeDate;
    private String approver;
    private String transport;
    private String food;
    private String others;
    private String currency;
    private String origin;
    private String destination;
    private String tDepartureDate;
    private String returnDate;
    private String city;
    private String arrivalDate;
    private String hDepartureDate;


    public SolicitudAnticipos(Map<String,String> anticipos){
        this.reasonForTheRequest = anticipos.get("reason_for_the_request");
        this.business = anticipos.get("business");
        this.brand = anticipos.get("brand");
        this.hometown = anticipos.get("hometown");
        this.depositeDate = anticipos.get("deposite_date");
        this.approver = anticipos.get("approver");
        this.transport = anticipos.get("transport");
        this.food = anticipos.get("food");
        this.others = anticipos.get("others");;
        this.currency = anticipos.get("currency");;
        this.origin = anticipos.get("origin");;
        this.destination = anticipos.get("destination");;
        this.tDepartureDate = anticipos.get("t_departure_date");;
        this.returnDate = anticipos.get("return_date");;
        this.city = anticipos.get("city");;
        this.arrivalDate = anticipos.get("arrival_date");;
        this.hDepartureDate = anticipos.get("h_departure_date");;

    }

    public String getReasonForTheRequest() {
        return reasonForTheRequest;
    }

    public String getBusiness() {
        return business;
    }

    public String getBrand() {
        return brand;
    }

    public String getHometown() {
        return hometown;
    }

    public String getDepositeDate() {
        return depositeDate;
    }

    public String getApprover() {
        return approver;
    }

    public String getTransport() {
        return transport;
    }

    public String getFood() {
        return food;
    }

    public String getOthers() {
        return others;
    }

    public String getCurrency() {
        return currency;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public String getTDepartureDate() {
        return tDepartureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public String getCity() {
        return city;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public String gethDepartureDate() {
        return hDepartureDate;
    }
}
