package solicitudes.models;



import java.util.List;
import java.util.Map;

public class Credenciales {

    private String mail;
    private String pass;

    public Credenciales(List<Map<String, String>> datos) {
        this.mail = datos.get(0).get("mail");
        this.pass = datos.get(0).get("pass");
    }

    public String getMail() {
        return mail;
    }

    public String getPass() {
        return pass;
    }
}
