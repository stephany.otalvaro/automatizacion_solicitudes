package solicitudes.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SendKeys;
import org.openqa.selenium.Keys;
import solicitudes.interactions.Wait;
import solicitudes.models.SolicitudAnticipos;
import solicitudes.utils.ObtenerDatos;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static solicitudes.userinterfaces.CreateRequest.*;
import static solicitudes.utils.Constant.CREATE_REQUEST_PATCH;

public class CreateRequest implements Task {
    private SolicitudAnticipos solicitudAnticipos;

    public CreateRequest() {
        this.solicitudAnticipos = new SolicitudAnticipos(ObtenerDatos.leerArchivo(CREATE_REQUEST_PATCH, 0));
    }

    @Override
    public <T extends Actor> void performAs(T actor) {


        actor.attemptsTo(Click.on(ADVANCE_PAYMENT_REQUEST),
                Enter.theValue(solicitudAnticipos.getReasonForTheRequest()).into(REASON),
                Click.on(BUSINESS),
                Enter.theValue(solicitudAnticipos.getBusiness()).into(BUSINESS).thenHit(Keys.ENTER),
                Click.on(MARK),
                Enter.theValue(solicitudAnticipos.getBrand()).into(MARK).thenHit(Keys.ENTER),
                Click.on(ACCEPT),
                Wait.theSeconds(2),
                Click.on(CITY),
                Enter.theValue(solicitudAnticipos.getCity()).into(CITY),
                Wait.theSeconds(2),
                SendKeys.of(Keys.ENTER).into(CITY),
                Wait.theSeconds(2),
                Click.on(DEPOSITE_DATE),
                Enter.theValue(solicitudAnticipos.getDepositeDate()).into(DEPOSITE_DATE).thenHit(Keys.ENTER),
                Click.on(APPROVER),
                Enter.theValue(solicitudAnticipos.getApprover()).into(APPROVER).thenHit(Keys.ENTER),
                Click.on(TRANSPORT),
                Enter.theValue(solicitudAnticipos.getTransport()).into(TRANSPORT).thenHit(Keys.ENTER),
                Click.on(FOOD),
                Enter.theValue(solicitudAnticipos.getFood()).into(FOOD).thenHit(Keys.ENTER),
                Click.on(OTHERS),
                Enter.theValue(solicitudAnticipos.getOthers()).into(OTHERS).thenHit(Keys.ENTER),
                Click.on(CURRENCY),
                Enter.theValue(solicitudAnticipos.getCurrency()).into(CURRENCY).thenHit(Keys.ENTER),
                Click.on(ROUND_TRIP),
                Wait.theSeconds(2),
                Click.on(ORIGIN),
                Enter.theValue(solicitudAnticipos.getOrigin()).into(ORIGIN),
                Wait.theSeconds(2),
                SendKeys.of(Keys.ENTER).into(ORIGIN),
                Wait.theSeconds(2),
                Click.on(DESTINATION),
                Enter.theValue(solicitudAnticipos.getDestination()).into(DESTINATION),
                Wait.theSeconds(2),
                SendKeys.of(Keys.ENTER).into(DESTINATION),
                Wait.theSeconds(2),
                Click.on(DEPARTURE_DATE),
                Enter.theValue(solicitudAnticipos.getTDepartureDate()).into(DEPARTURE_DATE),
                Wait.theSeconds(2),
                SendKeys.of(Keys.ENTER).into(DEPARTURE_DATE),
                Wait.theSeconds(2),
                Click.on(RETURN_DATE),
                Enter.theValue(solicitudAnticipos.getReturnDate()).into(RETURN_DATE),
                Wait.theSeconds(2),
                SendKeys.of(Keys.ENTER).into(RETURN_DATE),
                Wait.theSeconds(2),
                Click.on(HOSTING_IN_CITY),
                Enter.theValue(solicitudAnticipos.getCity()).into(HOSTING_IN_CITY),
                SendKeys.of(Keys.ENTER).into(HOSTING_IN_CITY),
                Click.on(ARRIVAL_DATE),
                Enter.theValue(solicitudAnticipos.getArrivalDate()).into(ARRIVAL_DATE).thenHit(Keys.ENTER),
                Click.on(DEPARTURE_DATE_HOSTING),
                Enter.theValue(solicitudAnticipos.gethDepartureDate()).into(DEPARTURE_DATE_HOSTING).thenHit(Keys.ENTER),
                Click.on(ADD),
                Click.on(CREATE_REQUEST),
                Wait.theSeconds(2)


        );


    }

    public static CreateRequest create() {
        return instrumented(CreateRequest.class);

    }
}
