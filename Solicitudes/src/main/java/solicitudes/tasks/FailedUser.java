package solicitudes.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import solicitudes.models.Credenciales;
import solicitudes.userinterfaces.SuccefulUser;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class FailedUser implements Task {

    private Credenciales credenciales;

    public FailedUser(Credenciales credenciales) {

        this.credenciales = credenciales;

    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(SuccefulUser.WELCOME),
                Enter.theValue(credenciales.getMail()).into(SuccefulUser.EMAIL),
                Click.on(SuccefulUser.NEXT)
        );

    }

    public static FailedUser fail(Credenciales credenciales) {
        return instrumented(FailedUser.class, credenciales);
    }

}
