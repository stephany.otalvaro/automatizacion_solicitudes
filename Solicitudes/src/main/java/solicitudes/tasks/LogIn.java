package solicitudes.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import solicitudes.interactions.Wait;
import solicitudes.models.Credenciales;
import solicitudes.userinterfaces.SuccefulUser;
import solicitudes.utils.LeerCodigos;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LogIn implements Task {

    private Credenciales credenciales;
    private String codigoGenerado;

    public LogIn(Credenciales credenciales) {

        this.credenciales = credenciales;

    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        LeerCodigos codigos = new LeerCodigos();
        try {
            codigoGenerado = String.valueOf(codigos.inicio());
        } catch (Exception e) {
            e.printStackTrace();

        }
        System.out.println(codigoGenerado);

        actor.attemptsTo(Click.on(SuccefulUser.WELCOME),
                Enter.theValue(credenciales.getMail()).into(SuccefulUser.EMAIL),
                Click.on(SuccefulUser.NEXT),
                WaitUntil.the(SuccefulUser.EMAIL, WebElementStateMatchers.isClickable()),
                Enter.theValue(credenciales.getPass()).into(SuccefulUser.PASSWORD),
                Click.on(SuccefulUser.NEXT_2),
                Wait.theSeconds(2));
        if (SuccefulUser.FAIL_ALERT.resolveFor(actor).isVisible()) {
            actor.attemptsTo(Click.on(SuccefulUser.TRY_ANOTHER_WAY_ALERT),
                    Wait.theSeconds(2),
                    Click.on(SuccefulUser.EIGHT_DIGIT_CODES),
                    Enter.theValue(codigoGenerado).into(SuccefulUser.WRITE_A_CODE),
                    Click.on(SuccefulUser.NEXT_3));

        } else {
            actor.attemptsTo(Click.on(SuccefulUser.TRY_ANOTHER_WAY),
                    Wait.theSeconds(2),
                    Click.on(SuccefulUser.EIGHT_DIGIT_CODES),
                    Enter.theValue(codigoGenerado).into(SuccefulUser.WRITE_A_CODE),
                    Click.on(SuccefulUser.NEXT_3)

            );
        }


        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }


    }

    public static LogIn pragma(Credenciales credenciales) {
        return instrumented(LogIn.class, credenciales);

    }
}
