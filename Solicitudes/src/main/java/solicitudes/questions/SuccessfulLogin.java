package solicitudes.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import solicitudes.userinterfaces.SuccefulUser;

public class SuccessfulLogin implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(SuccefulUser.MESSAGE_FINAL).viewedBy(actor).asString();
    }

    public static SuccessfulLogin login() {

        return new SuccessfulLogin();
    }
}
