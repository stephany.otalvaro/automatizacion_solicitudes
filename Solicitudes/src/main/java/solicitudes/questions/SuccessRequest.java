package solicitudes.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.waits.WaitUntil;
import solicitudes.userinterfaces.CreateRequest;
import solicitudes.utils.LeerCodigos;

import java.io.IOException;

import static solicitudes.userinterfaces.CreateRequest.LAST_REQUEST;
import static solicitudes.userinterfaces.CreateRequest.SUCCESS_REQUEST;

public class SuccessRequest implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        actor.attemptsTo(
                WaitUntil.the(SUCCESS_REQUEST, WebElementStateMatchers.isPresent()).forNoMoreThan(5).seconds());
        try {
            WaitUntil.the(LAST_REQUEST, WebElementStateMatchers.isVisible()).forNoMoreThan(10).seconds();
            LeerCodigos.guardarSolicitudId(Text.of(LAST_REQUEST).viewedBy(actor).asString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return Text.of(CreateRequest.SUCCESS_REQUEST).viewedBy(actor).asString();
    }

    public static SuccessRequest validate() {
        return new SuccessRequest();
    }
}
