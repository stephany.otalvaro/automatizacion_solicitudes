package solicitudes.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import solicitudes.userinterfaces.SuccefulUser;

public class FinalMessage implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {

        return Text.of(SuccefulUser.ERROR_LOGIN).viewedBy(actor).asString();
    }

    public static FinalMessage order() {
        return new FinalMessage();
    }
}
