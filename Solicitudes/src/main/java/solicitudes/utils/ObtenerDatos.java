package solicitudes.utils;

import net.thucydides.core.steps.stepdata.CSVTestDataSource;
import net.thucydides.core.steps.stepdata.TestDataSource;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ObtenerDatos {

    public static Map<String, String> leerArchivo(String ruta, int registro)  {
        TestDataSource datoscsv = null;
        try {
            datoscsv = new CSVTestDataSource(ruta);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<Map<String, String>> datos = datoscsv.getData();

        return datos.get(registro);

    }
    public static Map<String, String> leerArchivo(String ruta) throws IOException {
        TestDataSource datoscsv = new CSVTestDataSource(ruta);
        List<Map<String, String>> datos = datoscsv.getData();
        int registro = (int)((Math.random() * (datos.size() - 0)) + 0);
        System.out.println(registro);


        return datos.get(registro);

    }


}



