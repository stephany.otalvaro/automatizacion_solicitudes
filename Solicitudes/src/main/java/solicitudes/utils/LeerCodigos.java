package solicitudes.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class LeerCodigos {
    static int leer;
    static String obtenidoAleatorio;

    static List<Integer> valoresObtenidos;

    public Object inicio() throws Exception {

        valoresObtenidos = leerTexto("src\\test\\resources\\codigos\\codigos.txt");
        obtenidoAleatorio = obtenerCodigoAleatorio(valoresObtenidos.toArray(new Integer[]{}));
        borrarCodigoUtilizado("src\\test\\resources\\codigos\\codigos.txt");

        return obtenidoAleatorio;
    }


    private static List<Integer> leerTexto(String ruta) throws FileNotFoundException {

        InputStream ins = new FileInputStream(ruta);
        Scanner obj = new Scanner(ins);

        List<Integer> valoresObtenidos = new ArrayList<Integer>();

        while (obj.hasNextLine()) {
            valoresObtenidos.add(Integer.valueOf(obj.nextLine()));
        }
        return valoresObtenidos;

    }

    public static String obtenerCodigoAleatorio(Integer[] codigos) {
        int indiceAleatorio = numeroAleatorioEnRango(0, codigos.length - 1);
        int codigoAleatorio = codigos[indiceAleatorio];
        String numCadena = String.valueOf(codigoAleatorio);
        return numCadena;
    }

    public static int numeroAleatorioEnRango(int minimo, int maximo) {
        return ThreadLocalRandom.current().nextInt(minimo, maximo + 1);
    }

    public static List<Integer> borrarCodigoUtilizado(String ruta) throws IOException {
        int Buscar = Integer.valueOf(obtenidoAleatorio);
        int Remover = (valoresObtenidos.indexOf(Buscar));
        valoresObtenidos.remove(Remover);

        BufferedWriter bw = new BufferedWriter(new FileWriter(ruta));

        for (int i = 0; i < valoresObtenidos.size(); i++) {
            bw.write(String.valueOf(valoresObtenidos.get(i)));
            bw.newLine();
        }
        bw.close();
        return valoresObtenidos;
    }

    public static void guardarSolicitudId(String id) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("src\\test\\resources\\idsolicitudes\\idsolicitudes.txt"));
        bw.write(id);
        bw.close();


    }
}
